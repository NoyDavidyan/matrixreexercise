package com.example.matrixreexercise.fruits_list.api;

import com.example.matrixreexercise.model.FruitModel;
import java.util.ArrayList;

/**
 * Created by Noy davidyan on 02/11/2021.
 */

public class JSONResponse {

    private ArrayList<FruitModel> fruits;

    public ArrayList<FruitModel> getFruits() {
        return this.fruits;
    }
}
